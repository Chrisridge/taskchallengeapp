import { useEffect, useState } from "react";

function useFetch(pageNumber) {
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const res = await fetch(`https://reqres.in/api/users?delay=3&page=${pageNumber}/`);
        const json = await res.json();
        setData(json);
      } catch (e) {
        console.error(e);
      }
      setIsLoading(false);
    };
    fetchData()
  }, [pageNumber]);

  return {
    isLoading,
    data
  };
}

export default useFetch
