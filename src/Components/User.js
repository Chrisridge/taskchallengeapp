import React from 'react'
import useFetch from './Fetch';
import { useState, useEffect } from "react";
import TablePagination from './TablePagination';
import TableData from './TableData';

function Table() {
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const userFetchResponse = useFetch(currentPage);
  const [usersArr, setUsersArr] = useState([]);
  const [loading] = useState(false);

  useEffect(() => {
    if ('data' in userFetchResponse) {
      const users = userFetchResponse.data.data;
      setUsersArr(users)
      setTotalPages(userFetchResponse.data.total_pages)
    }
  }, [userFetchResponse])

  if (!usersArr || userFetchResponse.isLoading) {
    return 'Loading...';
  }

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <div className='container mt-5'>
      <h1>Page {currentPage}</h1>
      <TableData users={usersArr} loading={loading} />
      <TablePagination currentPage={currentPage} totalPages={totalPages} paginate={paginate} />
    </div>
  )
}

export default Table
