import React from 'react';


function TablePagination({ currentPage, totalPages, paginate }) {

  const createPagination = () => {
    let links = []
    for (let i = 0; i < totalPages; i++) {
      const pageNumber = i + 1
      links.push(
        <li
          key={pageNumber}
          className={currentPage === pageNumber ? 'page-item active' : 'page-item'}
        >
          <a onClick={(e) => {
            e.preventDefault()
            paginate(pageNumber)
          }}
            href={pageNumber}
            className='page-link'>
            {pageNumber}
          </a>
        </li>
      )
    }
    return links
  }

  return (
    <div>
      <ul className='pagination'>
        {createPagination()}
      </ul>
    </div>
  );
};

export default TablePagination


