import React from 'react'
import { Table } from 'reactstrap';

function TableData(posts) {
  return (
    <Table dark>
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
        {posts.users.map(user => (
          <tr key={user.id}>
            <td>{user.first_name}</td>
            <td>{user.last_name}</td>
            <td>{user.email}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  )

}

export default TableData
